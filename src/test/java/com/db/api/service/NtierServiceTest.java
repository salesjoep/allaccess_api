package com.db.api.service;

import com.db.api.model.NtierException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

class NtierServiceTest {

    @Test
    @DisplayName("Test Should Pass When Comment Doesn't Contain Swear Words.")
    void containsNotASwearWord() throws NtierException {
        NtierService ntierService = new NtierService();
        Assertions.assertFalse(ntierService.checkForWord("This is a clean comment"));
    }

    @Test
    @DisplayName("Should Throw Exception When Exception Contains Swear Words")
    public void shouldFailWhenCommentsContainsSwearWord() throws NtierException {
        NtierService ntierService = new NtierService();
        NtierException exception = assertThrows(NtierException.class, () ->{
            ntierService.checkForWord("This comment contains word");
        });
        assertTrue(exception.getMessage().contains("Comment contains word"));
    }

    @Test
    @DisplayName("Readable Version Of: Test Should Pass When Comment Doesn't Contain Swear Words.")
    void shouldNotContainSwearWordsInsideComment(){
        NtierService ntierService = new NtierService();
        assertThatThrownBy(() -> {
            ntierService.checkForWord("This is a word comment");
        }).isInstanceOf(NtierException.class)
                .hasMessage("Comment contains word");
    }


}