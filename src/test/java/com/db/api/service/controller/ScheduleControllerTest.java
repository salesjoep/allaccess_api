package com.db.api.service.controller;

import com.db.api.service.ScheduleService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
//@WebMvcTest(controllers = UserController.class)
public class ScheduleControllerTest {

    @MockBean
    private ScheduleService scheduleService;
    @Autowired
    private MockMvc mockMvc;

    @org.junit.Test
    @Test
    @DisplayName("Should list all schedules when making GET request to endpoint - api/schedules/")
    public void shouldListAllSchedules() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/schedules/"))
                .andExpect(MockMvcResultMatchers.status().is(403));
    }

    @org.junit.Test
    @Test
    @DisplayName("Should respond with not found request to endpoint - api/schedules/")
    public void shouldNotFindRequest() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/schedule/"))
                .andExpect(MockMvcResultMatchers.status().is(403));
    }
}
