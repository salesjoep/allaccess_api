package com.db.api.service.crud;

import com.db.api.repository.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CrudIntegrationTest {
    String jsonFormat = "{'username':'joep','password':'sales'}".replace("'", "\"");
    String newUserRequest = "{'username':'new','password':'user'}".replace("'", "\"");
    String newCalendarRequest = "{'month':'Januari','days':'31','year':'2021'}".replace("'", "\"");
    String updateScheduleRequest = "{'employee_Id':40,'year':2021,'month':'Januari','day':8}".replace("'", "\"");
    String newEvent = "{'id':'37','start':'','end':'','title':'','all_day':''}".replace("'", "\"");

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository repository;

    //Create user
    @org.junit.Test
    @Test
    @Sql("classpath:test-data.sql")
    @DisplayName("Test-if-we-can-create-a-user-without-login")
    public void shoudNotBeAbleToCreateUsers() throws Exception{
        mvc.perform(MockMvcRequestBuilders.post("/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newUserRequest))
                .andExpect(status().isForbidden());
    }

    //Get calendars
    @Test
    @org.junit.Test
    @DisplayName("Test-if-we-can-get-calendars-without-login")
    @Sql("classpath:test-data.sql")
    public void shouldNotBeAbleToGetCalendar() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/calendars")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    //Update schedules
    @Test
    @org.junit.Test
    @DisplayName("Test-if-we-can-update-schedule-without-login")
    @Sql("classpath:test-data.sql")
    public void shouldNotBeAbleToPutSchedule() throws Exception{
        mvc.perform(MockMvcRequestBuilders.put("/schedules/1/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(updateScheduleRequest))
                .andExpect(status().isForbidden());
    }

    //Delete events
    @Test
    @org.junit.Test
    @DisplayName("Test-if-we-can-delete-events-without-login")
    @Sql("classpath:test-data.sql")
    public void shouldNotBeAbleToDeleteEvent() throws Exception{
        mvc.perform(MockMvcRequestBuilders.delete("/events/30")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    //Create user with authentication
    @Test
    @org.junit.Test
    @DisplayName("Test-if-we-can-create-user-with-login")
    @Sql("classpath:test-data.sql")
    public void shouldBeAbleToCreateUser() throws Exception{
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonFormat))
                .andExpect(status().isOk())
                .andReturn();

        String jwt = result.getResponse().getContentAsString();
        StringBuilder sb = new StringBuilder(jwt);
        sb.delete(0, 10);
        sb.deleteCharAt(sb.length()-1);
        sb.deleteCharAt(sb.length()-1);
        String resultJwt = sb.toString();

        mvc.perform(MockMvcRequestBuilders.get("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + resultJwt))
                .andExpect(status().isOk());

        mvc.perform(MockMvcRequestBuilders.post("/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + resultJwt)
                .content(newUserRequest))
                .andExpect(status().isOk());
    }

    //Get calendars with authentication
    @Test
    @org.junit.Test
    @DisplayName("Test-if-we-can-get-calendars-with-login")
    @Sql("classpath:test-data.sql")
    public void shouldBeAbleToGetCalendars() throws Exception{

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonFormat))
                .andExpect(status().isOk())
                .andReturn();

        String jwt = result.getResponse().getContentAsString();
        StringBuilder sb = new StringBuilder(jwt);
        sb.delete(0, 10);
        sb.deleteCharAt(sb.length()-1);
        sb.deleteCharAt(sb.length()-1);
        String resultJwt = sb.toString();

        mvc.perform(MockMvcRequestBuilders.get("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + resultJwt))
                .andExpect(status().isOk());

        mvc.perform(MockMvcRequestBuilders.get("/calendars/")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + resultJwt))
                .andExpect(status().isOk());
    }

//    //Delete event with authentication
//    @org.junit.Test
//    @DisplayName("Test-if-we-can-delete-event-with-login")
//    @Sql("classpath:test-data.sql")
//    public void shouldBeAbleToDeleteEvent() throws Exception{
//        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/authenticate")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(jsonFormat))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        String jwt = result.getResponse().getContentAsString();
//        StringBuilder sb = new StringBuilder(jwt);
//        sb.delete(0, 10);
//        sb.deleteCharAt(sb.length()-1);
//        sb.deleteCharAt(sb.length()-1);
//        String resultJwt = sb.toString();
//
//        mvc.perform(MockMvcRequestBuilders.get("/login")
//                .contentType(MediaType.APPLICATION_JSON)
//                .header("Authorization", "Bearer " + resultJwt))
//                .andExpect(status().isOk());
//
//        mvc.perform(MockMvcRequestBuilders.post("/events/")
//        .contentType(MediaType.APPLICATION_JSON)
//                .content(newEvent)
//        .header("Authorization", "Bearer " + resultJwt))
//                .andExpect(status().isOk());
//
//
//        mvc.perform(MockMvcRequestBuilders.delete("/events/37")
//                .contentType(MediaType.APPLICATION_JSON)
//                .header("Authorization", "Bearer " + resultJwt))
//                .andExpect(status().isOk());
//    }
}
