package com.db.api.service.systemtests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

@ActiveProfiles("test")
public class CRUDSystemTests {

    @Test
    @Sql("classpath:test-data-events.sql")
    public void UserCanGetScheduleData() throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();
        opt.setHeadless(true);

        WebDriver driver = new ChromeDriver(opt);
        driver.get("https://allaccess-vue.herokuapp.com/#/login");

        //Maximize Window
        driver.manage().window().maximize();

        Thread.sleep(2000);

        //Find username
        driver.findElement(By.id("username"))
                .sendKeys("hello");

        //Find password
        driver.findElement(By.id("password"))
                .sendKeys("world");

        Thread.sleep(2000);

        //Clicking on the login button
        driver.findElement(By.id("login-button")).click();

        Thread.sleep(2000);

        //Navigate to schedule endpoint
        driver.get("https://allaccess-vue.herokuapp.com/#/schedule");
        Thread.sleep(2000);
        driver.navigate().refresh();

        Thread.sleep(2000);
        // Getting text of an element to check if user is logged in.
        String scheduleText = driver.findElement(By.className("schedule")).getText();
        Assertions.assertEquals("ADMIN PANEL - SCHEDULE", scheduleText);

        driver.close();


    }

    @Test
    public void UserCanGetEmployeeData() throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();
        opt.setHeadless(true);

        WebDriver driver = new ChromeDriver(opt);
        driver.get("https://allaccess-vue.herokuapp.com/#/login");

        //Maximize Window
        driver.manage().window().maximize();

        Thread.sleep(2000);

        //Find username
        driver.findElement(By.id("username"))
                .sendKeys("hello");

        //Find password
        driver.findElement(By.id("password"))
                .sendKeys("world");

        Thread.sleep(2000);

        //Clicking on the login button
        driver.findElement(By.id("login-button")).click();

        Thread.sleep(2000);

        //Navigate to schedule endpoint
        driver.get("https://allaccess-vue.herokuapp.com/#/employees");
        Thread.sleep(2000);
        driver.navigate().refresh();

        Thread.sleep(2000);
        // Getting text of an element to check if user is logged in.
        String scheduleText = driver.findElement(By.id("mypass")).getText();
        Assertions.assertEquals("sales", scheduleText);

        driver.close();


    }

    @Test
    public void UserCanCreateNewUsers() throws Exception{
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();
        opt.setHeadless(true);

        WebDriver driver = new ChromeDriver(opt);
        driver.get("https://allaccess-vue.herokuapp.com/#/login");

        //Maximize Window
        driver.manage().window().maximize();

        Thread.sleep(2000);

        //Find username
        driver.findElement(By.id("username"))
                .sendKeys("hello");

        //Find password
        driver.findElement(By.id("password"))
                .sendKeys("world");

        Thread.sleep(2000);

        //Clicking on the login button
        driver.findElement(By.id("login-button")).click();

        Thread.sleep(2000);

        //Navigate to schedule endpoint
        driver.get("https://allaccess-vue.herokuapp.com/#/employees");
        Thread.sleep(2000);
        driver.navigate().refresh();

        driver.findElement(By.id("username"))
                .sendKeys("SeleniumTestUser");

        Thread.sleep(2000);

        driver.findElement(By.id("password"))
                .sendKeys("password");

        driver.findElement(By.id("createUser")).click();

        Thread.sleep(2000);

        driver.navigate().refresh();

        Thread.sleep(2000);

        driver.findElement(By.id("deleteButton")).click();
        Thread.sleep(2000);
        // Getting text of an element to check if user is logged in.
        driver.findElement(By.className("swal2-confirm")).click();

        Thread.sleep(2000);



        String deletedText = driver.findElement(By.id("swal2-title")).getText();
        Assertions.assertEquals("Deleted!", deletedText);

        driver.close();
    }

}
