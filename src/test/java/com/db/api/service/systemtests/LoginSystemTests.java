package com.db.api.service.systemtests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

@ActiveProfiles("prod")
public class LoginSystemTests {

    @Test
    @Sql("classpath:test-data.sql")
    public void UserLogIn() throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();
        opt.setHeadless(true);
        //System.setProperty("webdriver.chrome.driver", "C:\\webdrivers\\chromedriver.exe");

        WebDriver driver = new ChromeDriver(opt);
        driver.get("https://allaccess-vue.herokuapp.com/#/login");

        //Maximize Window
        driver.manage().window().maximize();

        Thread.sleep(2000);

        //Find username
        driver.findElement(By.id("username"))
                .sendKeys("hello");

        //Find password
        driver.findElement(By.id("password"))
                .sendKeys("world");

        Thread.sleep(2000);

        //Clicking on the login button
        driver.findElement(By.id("login-button")).click();

        Thread.sleep(2000);

        // Getting text of an element to check if user is logged in.
        String homeText = driver.findElement(By.className("home")).getText();
        Assertions.assertEquals("ADMIN PANEL - HOME", homeText);

        driver.close();


    }

    @Test
    public void userLoginFail() throws InterruptedException{
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();
        opt.setHeadless(true);
        //System.setProperty("webdriver.chrome.driver", "C:\\webdrivers\\chromedriver.exe");

        WebDriver driver = new ChromeDriver(opt);
        driver.get("https://allaccess-vue.herokuapp.com/#/login");

        //Maximize Window
        driver.manage().window().maximize();

        Thread.sleep(2000);

        //Find username
        driver.findElement(By.id("username"))
                .sendKeys("nonExistingUser");

        //Find password
        driver.findElement(By.id("password"))
                .sendKeys("password");

        Thread.sleep(2000);

        //Clicking on the login button
        driver.findElement(By.id("login-button")).click();

        Thread.sleep(7000);

        // Getting text of an element to check if user is logged in.
        String errorText = driver.findElement(By.className("swal2-title")).getText();
        Assertions.assertEquals("Error", errorText);

        driver.close();
    }

    @Test
    public void userCanNavigateToDifferentPage() throws InterruptedException{
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();
        opt.setHeadless(true);
        //System.setProperty("webdriver.chrome.driver", "C:\\webdrivers\\chromedriver.exe");

        WebDriver driver = new ChromeDriver(opt);
        driver.get("https://allaccess-vue.herokuapp.com/#/login");

        //Maximize Window
        driver.manage().window().maximize();

        Thread.sleep(2000);

        //Find username
        driver.findElement(By.id("username"))
                .sendKeys("joep");

        //Find password
        driver.findElement(By.id("password"))
                .sendKeys("sales");

        Thread.sleep(2000);

        //Clicking on the login button
        driver.findElement(By.id("login-button")).click();

        Thread.sleep(7000);

        driver.get("https://allaccess-vue.herokuapp.com/#/employees");
        // Getting text of an element to check if user can navigate to different pages.
        String employeeText = driver.findElement(By.className("employees")).getText();
        Assertions.assertEquals("ADMIN PANEL - EMPLOYEES", employeeText);

        driver.close();
    }

    @Test
    public void userCanNavigateToScheduleEndpoint() throws InterruptedException{
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();
        opt.setHeadless(true);
        //System.setProperty("webdriver.chrome.driver", "C:\\webdrivers\\chromedriver.exe");

        WebDriver driver = new ChromeDriver(opt);
        driver.get("https://allaccess-vue.herokuapp.com/#/login");

        //Maximize Window
        driver.manage().window().maximize();

        Thread.sleep(2000);

        //Find username
        driver.findElement(By.id("username"))
                .sendKeys("joep");

        //Find password
        driver.findElement(By.id("password"))
                .sendKeys("sales");

        Thread.sleep(2000);

        //Clicking on the login button
        driver.findElement(By.id("login-button")).click();

        Thread.sleep(7000);

        driver.get("https://allaccess-vue.herokuapp.com/#/schedule");
        // Getting text of an element to check if user can navigate to different pages.
        String scheduleText = driver.findElement(By.className("schedule")).getText();
        Assertions.assertEquals("ADMIN PANEL - SCHEDULE", scheduleText);

        driver.close();
    }
}
