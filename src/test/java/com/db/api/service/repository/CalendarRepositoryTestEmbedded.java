package com.db.api.service.repository;

import com.db.api.model.Calendar;
import com.db.api.repository.CalendarRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
public class CalendarRepositoryTestEmbedded {
    @Autowired
    private CalendarRepository calendarRepository;

    @Test
    public void saveCalendar(){
        Calendar calendar = new Calendar(0, "test month", 31, 2020);
        Calendar savedCalendar = calendarRepository.save(calendar);
        assertThat(savedCalendar).usingRecursiveComparison().ignoringFields("id").isEqualTo(calendar);
    }

    @Test
    @Sql("classpath:test-data-calendar.sql")
    public void shouldSaveCalendarsThroughSqlFile(){
        Optional<Calendar> test = calendarRepository.findById(10);
        assertThat(test).isNotEmpty();
    }
}
