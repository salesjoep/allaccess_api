package com.db.api.service.repository;

import com.db.api.model.Schedule;
import com.db.api.repository.ScheduleRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
public class ScheduleRepositoryTestEmbedded {
    @Autowired
    private ScheduleRepository scheduleRepository;

    @Test
    public void saveSchedule(){
        Schedule schedule = new Schedule(0, 0, 2020, "Test Month", 10);
        Schedule savedSchedule = scheduleRepository.save(schedule);
        assertThat(savedSchedule).usingRecursiveComparison().ignoringFields("id").isEqualTo(schedule);
    }

    @Test
    @Sql("classpath:test-data-schedule.sql")
    public void shouldSaveScheduleThroughSqlFile(){
        Optional<Schedule> test = scheduleRepository.findById(10);
        assertThat(test).isNotEmpty();
    }
}
