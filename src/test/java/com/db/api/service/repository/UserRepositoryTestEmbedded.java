package com.db.api.service.repository;

import com.db.api.model.UserDao;
import com.db.api.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
public class UserRepositoryTestEmbedded {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void shouldSaveUser(){
        UserDao user = new UserDao( 0, "Test", "User");
        UserDao savedUser = userRepository.save(user);
        assertThat(savedUser).usingRecursiveComparison().ignoringFields("id").isEqualTo(user);
    }

    @Test
    @Sql("classpath:test-data.sql")
    public void shouldSaveUsersThroughSqlFile(){
        Optional<UserDao> test = userRepository.findById(10);
        assertThat(test).isNotEmpty();
    }

}
