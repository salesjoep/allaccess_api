DROP TABLE IF EXISTS myusers;

CREATE TABLE myusers (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  username VARCHAR(250) NOT NULL,
  password VARCHAR(250) NOT NULL
);

INSERT INTO myusers (username, password) VALUES
  ('joep', 'sales');