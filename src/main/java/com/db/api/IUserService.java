package com.db.api;

import com.db.api.model.UserDao;
import com.db.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface IUserService {

    List<UserDao> listAllUser();
    void saveUser(UserDao user);
    UserDao getUser(Integer id);
    void deleteUser(Integer id);


}
