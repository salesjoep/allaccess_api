package com.db.api.service;

import com.db.api.model.Schedule;
import com.db.api.repository.ScheduleRepository;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Configuration
public class ScheduleService {
    @Autowired
    private ScheduleRepository scheduleRepository;
    public List<Schedule> listAllSchedule(){
        return scheduleRepository.findAll();
    }
    public void saveSchedule(Schedule schedule){
        scheduleRepository.save(schedule);
    }

    public Schedule getSchedule(Integer id){
        return scheduleRepository.findById(id).get();
    }

    public void deleteSchedule(Integer id){
        scheduleRepository.deleteById(id);
    }
}
