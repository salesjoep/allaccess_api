package com.db.api.service;

import com.db.api.IUserService;
import com.db.api.model.UserDao;
import com.db.api.repository.UserRepository;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Configuration
public class UserService implements IUserService {
    @Autowired
    private UserRepository userRepository;
    public List<UserDao> listAllUser(){
        return userRepository.findAll();
    }
    public void saveUser(UserDao user){
        userRepository.save(user);
    }

    public UserDao getUser(Integer id){
        return userRepository.findById(id).get();
    }

    public UserDao getUserByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public void deleteUser(Integer id){
        userRepository.deleteById(id);
    }
}
