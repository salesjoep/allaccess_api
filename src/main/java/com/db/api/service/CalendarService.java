package com.db.api.service;

import com.db.api.model.Calendar;
import com.db.api.repository.CalendarRepository;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Configuration
public class CalendarService {
    @Autowired
    private CalendarRepository calendarRepository;
    public List<Calendar> listAllCalendar(){
        return calendarRepository.findAll();
    }
    public void saveCalendar(Calendar calendar){
        calendarRepository.save(calendar);
    }

    public Calendar getCalendar(Integer id){
        return calendarRepository.findById(id).get();
    }

    public void deleteCalendar(Integer id){
        calendarRepository.deleteById(id);
    }
}
