package com.db.api.service;

import com.db.api.IUserService;
import com.db.api.model.UserDao;
import com.db.api.repository.UserRepository;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserRepository repository;

    public UserDao getUserByUsername(String username){
        return repository.findByUsername(username);
    }

    @Override
    public List<UserDao> listAllUser() {
        return null;
    }

    @Override
    public void saveUser(UserDao user) {

    }

    @Override
    public UserDao getUser(Integer id) {
        return null;
    }

    @Override
    public void deleteUser(Integer id) {

    }
}
