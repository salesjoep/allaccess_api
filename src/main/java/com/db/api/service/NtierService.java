package com.db.api.service;

import com.db.api.model.Ntier;
import com.db.api.model.NtierException;
import com.db.api.repository.NtierRepository;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Configuration
public class NtierService {
    @Autowired
    private NtierRepository ntierRepository;
    public List<Ntier> listAllNtier(){
        return ntierRepository.findAll();
    }
    public void saveNtier(Ntier ntier){
        ntierRepository.save(ntier);
    }

    public Ntier getNtier(Integer id){
        return ntierRepository.findById(id).get();
    }

    public void deleteNtier(Integer id){
        ntierRepository.deleteById(id);
    }


    //Only for testing
    public boolean checkForWord(String comment) throws NtierException {
        if (comment.contains("word")){
            throw new NtierException("Comment contains word");
        }
        return false;
    }
}
