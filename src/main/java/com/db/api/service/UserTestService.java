package com.db.api.service;

import com.db.api.model.UserTest;
import com.db.api.repository.UserTestRepository;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Configuration
public class UserTestService {
    @Autowired
    private UserTestRepository userTestRepository;
    public List<UserTest> listAllUser(){
        return userTestRepository.findAll();
    }
    public void saveUser(UserTest user){
        userTestRepository.save(user);
    }

    public UserTest getUser(Integer id){
        return userTestRepository.findById(id).get();
    }

    public void deleteUser(Integer id){
        userTestRepository.deleteById(id);
    }
}
