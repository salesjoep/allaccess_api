package com.db.api.service;

import com.db.api.model.Event;
import com.db.api.repository.EventRepository;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Configuration
public class EventService {
    @Autowired
    private EventRepository eventRepository;

    public List<Event> listAllEvent(){
        return eventRepository.findAll();
    }

    public void saveEvent(Event event){
        eventRepository.save(event);
    }

    public Event getEvent(Integer id){
        return eventRepository.findById(id).get();
    }

    public void deleteEvent(Integer id){
        eventRepository.deleteById(id);
    }
}
