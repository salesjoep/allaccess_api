package com.db.api.repository;

import com.db.api.model.Ntier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NtierRepository extends JpaRepository<Ntier, Integer> {
}
