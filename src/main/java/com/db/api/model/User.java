package com.db.api.model;


import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    public int id;
    public String firstName;
    public String lastName;
    public String password;
    public String email;
    public String name;

    public User(){
    }

    public User(int id, String firstName, String lastName, String passWord, String eMail, String Name){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = passWord;
        this.email = eMail;
        this.name = Name;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId(){return id;}
    public void setId(int value){id = value;}

    public String getFirstName(){return firstName;}
    public void setFirstName(String value){firstName = value;}

    public String getLastName(){return lastName;}
    public void setLastName(String value){lastName = value;}

    public String getPassword(){return  password;}
    public void setPassword(String value){password = value;}

    public String getEmail(){return email;}
    public void setEmail(String value){email = value;}

    public String getName(){return name;}
    public void setName(String value){name = value;}
}
