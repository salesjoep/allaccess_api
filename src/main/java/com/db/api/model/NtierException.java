package com.db.api.model;

public class NtierException extends Exception {

    //Empty Constructor:
    public NtierException(){

    }

    public NtierException(String message){
        super(message);
    }
}
