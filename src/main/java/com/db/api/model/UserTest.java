package com.db.api.model;


import javax.persistence.*;

@Entity
@Table(name = "usertest")
public class UserTest {
    public int id;
    public String name;

    public UserTest(){

    }

    public UserTest(int id, String name){
        this.id = id;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId(){return id;}
    public void setId(int value){id = value;}

    public String getName(){return name;}
    public void setName(String value){name = value;}
}
