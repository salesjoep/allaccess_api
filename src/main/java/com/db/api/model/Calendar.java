package com.db.api.model;

import javax.persistence.*;

@Entity
@Table(name = "calendar")
public class Calendar {
    public int id;
    public String month;
    public int days;
    public int year;

    public Calendar(){

    }

    public Calendar(int id, String month, int days, int year){
        this.id = id;
        this.month = month;
        this.days = days;
        this.year = year;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId(){return id;}
    public void setId(int value){id = value;}

    public String getMonth(){return month;}
    public void setMonth(String value){month = value;}

    public int getDays(){return days;}
    public void setDays(int value){days = value;}

    public int getYear(){return year;}
    public void setYear(int value){year = value;}
}
