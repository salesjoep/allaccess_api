package com.db.api.model;

import javax.persistence.*;

@Entity
@Table(name = "schedule")
public class Schedule {
    public int id;
    public int employeeId;
    public int year;
    public String month;
    public int day;

    public Schedule(){

    }

    public Schedule(int id, int employeeId, int year, String month, int day){
        this.id = id;
        this.employeeId = employeeId;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId(){return id;}
    public void setId(int value){id = value;}

    public int getEmployeeId(){return employeeId;}
    public void setEmployeeId(int value){employeeId = value;}

    public int getYear(){return year;}
    public void setYear(int value){year = value;}

    public String getMonth(){return month;}
    public void setMonth(String value){month = value;}

    public int getDay(){return day;}
    public void setDay(int value){day = value;}
}
