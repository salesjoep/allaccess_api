package com.db.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Ntier {
    private String layerName;
    private int nTierId;

    public Ntier(){

    }

    public Ntier(String layerName, int id){
        this.layerName = layerName;
        this.nTierId = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getnTierId(){return nTierId;}
    public void setnTierId(int value){nTierId = value;}

    public String getLayerName(){return layerName;}
    public void setLayerName(String value){layerName = value;}
}
