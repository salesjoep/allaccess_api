package com.db.api.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "events")
public class Event {
    public int id;
    public LocalDateTime start;
    public LocalDateTime end;
    public String title;
    public boolean all_day;

    public Event(){

    }

    public Event(int id, LocalDateTime start, LocalDateTime end, String title, boolean all_day){
        this.id = id;
        this.start = start;
        this.end = end;
        this.title = title;
        this.all_day = all_day;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId(){return id;}
    public void setId(int value){id = value;}

    public LocalDateTime getStart(){return start;}
    public void setStart(LocalDateTime value){start = value;}

    public LocalDateTime getEnd() {return end;}
    public void setEnd(LocalDateTime end){this.end = end;}

    public String getTitle(){return title;}
    public void setTitle(String title){this.title = title;}

    public boolean isAll_day(){return all_day; }
    public void setAll_day(boolean all_day){this.all_day = all_day;}
}
