package com.db.api.controller;


import com.db.api.model.Calendar;
import com.db.api.service.CalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/calendars")
public class CalendarController {
    @Autowired
    CalendarService calendarService;

    @GetMapping("")
    public List<Calendar> list() {
        return calendarService.listAllCalendar();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Calendar> get(@PathVariable Integer id) {
        try {
            Calendar calendar = calendarService.getCalendar(id);
            return new ResponseEntity<Calendar>(calendar, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Calendar>(HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping("/")
    public void add(@RequestBody Calendar calendar) {
        calendarService.saveCalendar(calendar);
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Calendar calendar, @PathVariable Integer id) {
        try {
            Calendar existCalendar = calendarService.getCalendar(id);
            calendar.setId(id);
            calendarService.saveCalendar(calendar);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {

        calendarService.deleteCalendar(id);
    }
}