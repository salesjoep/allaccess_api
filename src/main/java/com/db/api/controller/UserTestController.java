package com.db.api.controller;

import com.db.api.model.UserTest;
import com.db.api.service.UserTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/userstest")
public class UserTestController {
    @Autowired
    UserTestService userTestService;

    @GetMapping("")
    public List<UserTest> list() {
        return userTestService.listAllUser();
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserTest> get(@PathVariable Integer id) {
        try {
            UserTest user = userTestService.getUser(id);
            return new ResponseEntity<UserTest>(user, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<UserTest>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/")
    public void add(@RequestBody UserTest user) {
        userTestService.saveUser(user);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody UserTest user, @PathVariable Integer id) {
        try {
            UserTest existUser = userTestService.getUser(id);
            user.setId(id);
            userTestService.saveUser(user);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {

        userTestService.deleteUser(id);
    }
}
