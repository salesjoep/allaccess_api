package com.db.api.controller;
import com.db.api.model.Schedule;
import com.db.api.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/schedules")
public class ScheduleController {
    @Autowired
    ScheduleService scheduleService;

    @GetMapping("")
    public List<Schedule> list() {
        return scheduleService.listAllSchedule();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Schedule> get(@PathVariable Integer id) {
        try {
            Schedule schedule = scheduleService.getSchedule(id);
            return new ResponseEntity<Schedule>(schedule, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Schedule>(HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping("/")
    public void add(@RequestBody Schedule schedule) {
        scheduleService.saveSchedule(schedule);
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Schedule schedule, @PathVariable Integer id) {
        try {
            Schedule existSchedule = scheduleService.getSchedule(id);
            schedule.setId(id);
            scheduleService.saveSchedule(schedule);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {

        scheduleService.deleteSchedule(id);
    }
}
