package com.db.api.controller;

import com.db.api.model.Ntier;
import com.db.api.service.NtierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/ntiers")
public class NtierController {
    @Autowired
    NtierService ntierService;

    @GetMapping("")
    public List<Ntier> list() {
        return ntierService.listAllNtier();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Ntier> get(@PathVariable Integer id) {
        try {
            Ntier ntier = ntierService.getNtier(id);
            return new ResponseEntity<Ntier>(ntier, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Ntier>(HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping("/")
    public void add(@RequestBody Ntier ntier) {
        ntierService.saveNtier(ntier);
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Ntier ntier, @PathVariable Integer id) {
        try {
            Ntier existNtier = ntierService.getNtier(id);
            ntier.setnTierId(id);
            ntierService.saveNtier(ntier);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {

        ntierService.deleteNtier(id);
    }
}

