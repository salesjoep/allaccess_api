package com.db.api.controller;

import com.db.api.model.UserDao;
import com.db.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/register")
public class RegisterResource {

    @Autowired
    UserService userService;

    @PostMapping("/")
    public void add(@RequestBody UserDao user){
        userService.saveUser(user);
    }
}
